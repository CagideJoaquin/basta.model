import basta.structure.FilterFactory;
import basta.structure.Filter;
import org.junit.Assert;
import org.junit.Test;

public class FilterFactoryTest {

    private FilterFactory ff;

    public FilterFactoryTest()
    {
        this.ff = new FilterFactory();
    }


    @Test
    public final void getDetecterByName()
    {
        Filter f = ff.getFilter("DetecterFilter","nombres");
        Assert.assertEquals("DetecterFilter", f.getName());
    }

    @Test
    public final void getDetecterByType1()
    {
        Filter f = ff.getFilter("DetecterFilter","nombres");
        Assert.assertEquals("nombres", f.getType());
    }

    @Test
    public final void getDetecterByType2()
    {
        Filter f = ff.getFilter("DetecterFilter","negras");
        Assert.assertEquals("negras", f.getType());
    }

    @Test
    public final void getTranslateByName()
    {
        Filter f = ff.getFilter("TranslateFilter","vocals");
        Assert.assertEquals("TranslateFilter", f.getName());
    }

    @Test
    public final void getTranslateByType1()
    {
        Filter f = ff.getFilter("TranslateFilter","mayus");
        Assert.assertEquals("mayus", f.getType());
    }

    @Test
    public final void getTranslateByType2()
    {
        Filter f = ff.getFilter("TranslateFilter","vocals");
        Assert.assertEquals("vocals", f.getType());
    }

}
