import basta.filters.TranslateFilter;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TranslateFilterTest {

    private Map<Character,Character> mapeo;
    private TranslateFilter translateFilter;

    public TranslateFilterTest()
    {
        this.mapeo = new HashMap<>();

        mapeo.put('á','a');
        mapeo.put('é','e');
        mapeo.put('!',' ');
        mapeo.put('?',' ');
        mapeo.put('ü','u');
        mapeo.put('A','a');
        mapeo.put('B','b');

       this.translateFilter = new TranslateFilter("TranslateFilter", "General");
       this.translateFilter.configureFilter(mapeo);
    }

    @Test
    public final void deleteVocals() {

        Message message = new Message();

        message.setBody("Hola papá");
        message.addAttribute("fecha","13-04-19");

        translateFilter.getIQueueIn().put(message);

        translateFilter.filter();

        Assert.assertEquals("Hola papa",translateFilter.getIQueueOut().getMessage().getBody());
    }

    @Test
    public final void criteria1() {

        Message message = new Message();

        message.setBody("Ahora papá");
        message.addAttribute("fecha","16-04-19");

        translateFilter.getIQueueIn().put(message);

        translateFilter.filter();

        Assert.assertEquals("ahora papa",translateFilter.getIQueueOut().getMessage().getBody());
    }

    @Test
    public final void criteria2() {

        Message message = new Message();

        message.setBody("Buenos días señor pingüino!!!");
        message.addAttribute("fecha","16-04-19");

        translateFilter.getIQueueIn().put(message);

        translateFilter.filter();

        Assert.assertEquals("buenos días señor pinguino   ",translateFilter.getIQueueOut().getMessage().getBody());
    }

    @Test
    public final void noTranslate() {

        Message message = new Message();
        Message message2 = new Message();

        message.setBody("ahora papa");
        message.addAttribute("fecha","16-04-19");
        message2.setBody("ahora papa");
        message2.addAttribute("fecha","16-04-19");

        translateFilter.getIQueueIn().put(message2);

        translateFilter.filter();

        Assert.assertEquals(message.toString(),translateFilter.getIQueueOut().getMessage().toString());
    }
}
