import basta.connection.NetRegistry;
import basta.connection.PanelRegistry;
import basta.connection.netconfiguration.Configuration;
import basta.connection.netviews.TwitterPanel;
import basta.connection.networks.PageTwitter;
import basta.util.ConfigurationMnemonic;
import basta.util.NetNameMnemonic;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PluginTest {

    private TwitterPanel tp;
    private Configuration config;
    private PageTwitter pt;

    @Before
    public final void initData() {
        tp = new TwitterPanel();
        pt = new PageTwitter();
        config = new Configuration();
        config.putParameter(ConfigurationMnemonic.TOKEN_KEY, "clave1");
        config.putParameter(ConfigurationMnemonic.TOKEN_SECRET, "clave2");
        config.putParameter(ConfigurationMnemonic.CONSUMER_KEY, "clave3");
        config.putParameter(ConfigurationMnemonic.CONSUMER_SECRET, "clave4");


    }

    @Test
    public final void NetRegistryTest() {
        NetRegistry.getInstance().put(pt.getName(), pt);

        Assert.assertEquals(NetNameMnemonic.TWITTER, NetRegistry.getInstance().get("Twitter").getName());
    }

    @Test
    public final void PanelRegistryTest() {
        PanelRegistry.getInstance().put(tp.getName(), tp);

        Assert.assertEquals(NetNameMnemonic.TWITTER, PanelRegistry.getInstance().get("Twitter").getName());
    }

    @Test
    public void setConfiguration() {
        tp.setConfiguration(this.config);
        Configuration configurationAux = tp.getConfiguration();

        Assert.assertEquals(this.config.getParameter(ConfigurationMnemonic.CONSUMER_KEY), configurationAux.getParameter(ConfigurationMnemonic.CONSUMER_KEY));
    }
}
