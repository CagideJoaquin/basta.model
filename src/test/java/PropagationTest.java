
import basta.structure.Message;
import basta.structure.PipeFilter;
import basta.structure.channel.pubsubchannel.Event;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class PropagationTest {

    private final static String TWITTER = "TWITTER";
    private PipeFilter pipeFilter;

    @Before
    public final void initData() {

        pipeFilter = new PipeFilter();

        Event.operation.subscribe(TWITTER, pipeFilter );

    }


    @Test
    public final void publishZero() {

        Assert.assertTrue(pipeFilter.getIQueueIn().isEmpty());

    }

    @Test
    public final void publishOne() {

        Event.operation.publish(TWITTER,new Message());

        Assert.assertEquals(pipeFilter.getIQueueIn().size(),1);

    }

    @Test
    public final void publishTwo() {

        Event.operation.publish(TWITTER,new Message());
        Event.operation.publish(TWITTER,new Message());

        Assert.assertEquals(pipeFilter.getIQueueIn().size() , 2);

    }
}
