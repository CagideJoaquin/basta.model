import basta.connection.netconfiguration.Configuration;
import basta.connection.networks.PageTwitter;
import basta.inicialization.BastaModel;
import basta.structure.SequenceMaker;
import basta.structure.Sequencer;
import basta.structure.channel.pubsubchannel.Event;
import basta.util.ConfigurationMnemonic;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import twitter4j.TwitterException;

import java.io.IOException;

public class WindowSentimentAnalysisTest {

    private PageTwitter pageTwitter;
    private Sequencer sequence;

    @Before
    public final void initData() throws IOException {

        BastaModel.getInstance(false).init();


        pageTwitter = new PageTwitter();

        pageTwitter.setConfiguration(new Configuration());
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.CONSUMER_KEY, "irVWxKScCJKxqzefhbKZOajaL");
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.CONSUMER_SECRET, "jR56K2u2D5k8rgRkMF4y28CrTHu2QZnYz8CckTmh8tp1eLZWVu");
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.TOKEN_KEY, "1119972818404376576-pOGjc7Ms365EJ57xN4Fgpq6HTw9bAS");
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.TOKEN_SECRET, "bMHIyccv15J3yWwBrgFsNdKgL9vz1Uj1u8IxYFqVoOR54");

        pageTwitter.connect();


        SequenceMaker sequenceMaker = new SequenceMaker();
        sequence = sequenceMaker.makeSequence("seqWindowSentimentAnalysisTest.csv");

        Event.operation.subscribe(pageTwitter.getName(), sequence.getPipeFilter());


    }

    @Test
    public final void generateAlert() {

        try {
            pageTwitter.getMessagesStream();
        } catch (TwitterException e) {
            System.out.println("ERROR, NO INTERNET:\n" + e.getMessage());
        }

        sequence.getPipeFilter().filter();
        String result = sequence.getPipeFilter().getIQueueOut().getMessage().getBody();

        Assert.assertEquals("ALERT : -1",result);

    }

}
