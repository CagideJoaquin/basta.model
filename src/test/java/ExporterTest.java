import basta.exporter.Exporter;
import basta.exporter.StrategyExportTxt;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExporterTest {

    private Exporter exporter;
    private StrategyExportTxt strategy;
    private Message messageA;
    private Message messageB;

    public ExporterTest() {
        this.strategy = new StrategyExportTxt();
        this.exporter = new Exporter(strategy);
        this.messageA = new Message();
        this.messageB = new Message();

        messageA.setBody("Hola Soy el primer mensaje hermoso.");
        messageA.addAttribute("sequenceName", "Twitter");
        messageB.setBody("Hola Soy el segundo mensaje hermoso.");
        messageB.addAttribute("sequenceName", "Twitter");
    }

    @Test
    public final void criterio1() throws IOException {

        exporter.export(messageA);

        String result = strategy.lastLine(messageA.getHeaderMessage().getAttributes().get("sequenceName"));

        Files.deleteIfExists(Paths.get("Twittermessages.txt"));

        Assert.assertEquals("{\"body\":{\"value\":\"Hola Soy el primer mensaje hermoso.\"},\"headerMessage\":{\"attributes\":{\"sequenceName\":\"Twitter\"}}}", result);
    }

    @Test
    public final void criterio2() throws IOException {

        exporter.export(messageA);
        exporter.export(messageB);

        String result = strategy.lastLine(messageA.getHeaderMessage().getAttributes().get("sequenceName"));

        Files.deleteIfExists(Paths.get("Twittermessages.txt"));

        Assert.assertEquals("{\"body\":{\"value\":\"Hola Soy el primer mensaje hermoso.\"},\"headerMessage\":{\"attributes\":{\"sequenceName\":\"Twitter\"}}}{\"body\":{\"value\":\"Hola Soy el segundo mensaje hermoso.\"},\"headerMessage\":{\"attributes\":{\"sequenceName\":\"Twitter\"}}}", result);
    }

    @Test
    public final void criterio3() throws IOException {

        messageA.getHeaderMessage().getAttributes().remove("sequenceName");

        exporter.export(messageA);

        String result = strategy.lastLine(messageA.getHeaderMessage().getAttributes().get("sequenceName"));

        Files.deleteIfExists(Paths.get("Twittermessages.txt"));

        Assert.assertNull(result);
    }
}