import basta.structure.Message;
import basta.structure.SequenceMaker;
import basta.structure.Sequencer;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SequenceMakerTest {

    private SequenceMaker sequenceMaker;
    private Sequencer emptySequence, repeatedElementsSequence, oneElementSequence;

    public SequenceMakerTest() {
        this.sequenceMaker = new SequenceMaker();
        try {
            this.emptySequence = sequenceMaker.makeSequence("emptySequence.csv");
            this.repeatedElementsSequence = sequenceMaker.makeSequence("repeatedElementsSequence.csv");
            this.oneElementSequence = sequenceMaker.makeSequence("oneElementSequence.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Se consultan los tamaños de las secuencias de filtros y se debe obtener 0, 2 y 1.
    @Test
    public void criterio1() {

        Assert.assertEquals(0, emptySequence.getPipeFilter().getFilters().size());

        Assert.assertEquals(2, repeatedElementsSequence.getPipeFilter().getFilters().size());

        Assert.assertEquals(1, oneElementSequence.getPipeFilter().getFilters().size());
    }

    //Se consulta el nombre del filtro en la segunda posición de la repeatedElementsSequence y se debe obtener TranslateFilter.
    @Test
    public void criterio2() {

        Assert.assertEquals("TranslateFilter", repeatedElementsSequence.getPipeFilter().getFilters().get(1).getName());
    }

    //Se envía un mensaje a la secuencia de filtros y se espera que llegue a la cola de salida del último filtro en la secuencia.
    @Test
    public void criterio3() {

        Message message = new Message();
        message.setBody("en la casa de maria");

        repeatedElementsSequence.getPipeFilter().getIQueueIn().put(message);
        repeatedElementsSequence.getPipeFilter().filter();

        Assert.assertEquals(message.hashCode(), repeatedElementsSequence.getPipeFilter().getIQueueOut().getMessage().hashCode());
    }

    @Test(expected = FileNotFoundException.class)
    public void criterio4() throws IOException {
        emptySequence = sequenceMaker.makeSequence("");
    }
}