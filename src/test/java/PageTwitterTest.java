import basta.connection.netconfiguration.Configuration;
import basta.inicialization.BastaModel;
import basta.connection.networks.PageTwitter;
import basta.structure.SequenceMaker;
import basta.structure.Sequencer;
import basta.structure.channel.pubsubchannel.Event;
import basta.util.ConfigurationMnemonic;
import io.sniffy.socket.DisableSockets;
import io.sniffy.test.junit.SniffyRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import twitter4j.TwitterException;


import java.io.IOException;
import java.net.ConnectException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;


public class PageTwitterTest {

    private PageTwitter pageTwitter;
    @Rule public SniffyRule sniffyRule = new SniffyRule();

    @Before
    public final void initData()  {

        BastaModel.getInstance(false).init();

        pageTwitter = new PageTwitter();

        pageTwitter.setConfiguration(new Configuration());
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.CONSUMER_KEY, "irVWxKScCJKxqzefhbKZOajaL");
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.CONSUMER_SECRET, "jR56K2u2D5k8rgRkMF4y28CrTHu2QZnYz8CckTmh8tp1eLZWVu");
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.TOKEN_KEY, "1119972818404376576-pOGjc7Ms365EJ57xN4Fgpq6HTw9bAS");
        pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.TOKEN_SECRET, "bMHIyccv15J3yWwBrgFsNdKgL9vz1Uj1u8IxYFqVoOR54");

        pageTwitter.connect();

    }


    @Test
    public final void extractFiveTweetsByPage() throws IOException {

        SequenceMaker sequenceMaker = new SequenceMaker();
        Sequencer sequencer = sequenceMaker.makeSequence("seqPageTwitterTest.csv");

        Event.operation.subscribe(pageTwitter.getName(), sequencer.getPipeFilter());


        try {
            pageTwitter.getMessagesStream();
        } catch (TwitterException e) {
            System.out.println("ERROR, NO INTERNET:\n" + e.getMessage());
        }


        Assert.assertEquals(5, sequencer.getPipeFilter().getIQueueIn().size());
    }

    @Test
    @DisableSockets
    public void testDisableSockets() throws IOException {
        try {
            pageTwitter.connect();
            pageTwitter.getMessagesStream();
        } catch (TwitterException e) {
            System.out.println("ERROR, NO INTERNET:\n" + e.getMessage());
            assertNotNull(e);
        }
    }

    @Test
    public void testNoAccessNetwork(){
        try {
            pageTwitter.setConfiguration(new Configuration());
            pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.CONSUMER_KEY, "irVW");
            pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.CONSUMER_SECRET, "jR56u");
            pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.TOKEN_KEY, "111S");
            pageTwitter.getConfiguration().putParameter(ConfigurationMnemonic.TOKEN_SECRET, "bVoR54");

            pageTwitter.connect();
            pageTwitter.getMessagesStream();

            System.out.println(pageTwitter.getConnection());

        } catch (Exception e) {
            System.out.println("ERROR, NO ACCESS TO THE NETWORK :\n" + e.getMessage());
            assertNotNull(e);
        }
    }
}
