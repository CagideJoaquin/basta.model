import basta.filters.DetecterFilter;
import basta.filters.IStrategyDetect;
import basta.filters.StrategyDetectChars;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DetecterFilterTest {

    private List<String> tokens;
    private DetecterFilter detecterWords;
    private StrategyDetectChars stratChars;

    public DetecterFilterTest() {
        this.tokens = new ArrayList<>();
        tokens.add("maria");
        tokens.add("julia");
        this.stratChars = new StrategyDetectChars();
        this.detecterWords = new DetecterFilter("DetecterFilter", "Default");
        detecterWords.configureFilter(tokens);
    }

    @Test
    public final void emptyMessage() {

        Message message = new Message();

        message.setBody("");
        message.addAttribute("fecha", "13-04-19");

        detecterWords.getIQueueIn().put(message);

        detecterWords.filter();

        Assert.assertEquals(null, detecterWords.getIQueueOut().getMessage());
    }

    @Test
    public final void detectWord1() {

        Message message = new Message();

        message.setBody("maria esta de fiesta");
        message.addAttribute("fecha", "13-04-19");

        detecterWords.getIQueueIn().put(message);

        detecterWords.filter();

        Assert.assertEquals("maria esta de fiesta", detecterWords.getIQueueOut().getMessage().getBody());
    }

    @Test
    public final void detectWord2() {

        Message message = new Message();

        message.setBody("en la casa de carla");
        message.addAttribute("fecha", "13-04-19");

        detecterWords.getIQueueIn().put(message);

        detecterWords.filter();

        Assert.assertEquals(null, detecterWords.getIQueueOut().getMessage());
    }

    @Test
    public final void detectChars1() {

        this.detecterWords.changeStrategy(stratChars);

        Message message = new Message();

        message.setBody("en la casa de jul");
        message.addAttribute("fecha", "13-04-19");

        detecterWords.getIQueueIn().put(message);

        detecterWords.filter();

        Assert.assertEquals("en la casa de jul", detecterWords.getIQueueOut().getMessage().getBody());
    }

    @Test
    public final void detectChars2() {

        this.detecterWords.changeStrategy(stratChars);

        Message message = new Message();

        message.setBody("en la casa de ma");
        message.addAttribute("fecha", "13-04-19");

        detecterWords.getIQueueIn().put(message);

        detecterWords.filter();

        Assert.assertEquals(null, detecterWords.getIQueueOut().getMessage());
    }

    @Test
    public final void detectWordNoTokens() {

        this.detecterWords.configureFilter(new ArrayList<>());

        Message message = new Message();

        message.setBody("maria esta de fiesta");
        message.addAttribute("fecha", "13-04-19");

        detecterWords.getIQueueIn().put(message);

        detecterWords.filter();

        Assert.assertEquals("maria esta de fiesta", detecterWords.getIQueueOut().getMessage().getBody());
    }

    @Test
    public final void detectCharsNoTokens() {

        this.detecterWords.configureFilter(new ArrayList<>());

        this.detecterWords.changeStrategy(stratChars);

        Message message = new Message();

        message.setBody("maria esta de fiesta");
        message.addAttribute("fecha", "13-04-19");

        detecterWords.getIQueueIn().put(message);

        detecterWords.filter();

        Assert.assertEquals("maria esta de fiesta", detecterWords.getIQueueOut().getMessage().getBody());
    }
}
