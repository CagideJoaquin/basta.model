import basta.filters.DetecterFilter;
import basta.filters.ProxyClassLoader;
import basta.structure.Filter;
import org.junit.Assert;
import org.junit.Test;

public class FilterRetrieverTest {


    ProxyClassLoader proxy;

    public FilterRetrieverTest()
    {
        this.proxy = new ProxyClassLoader();
    }

    @Test
    public final void getExistingClass() {

        DetecterFilter dec = new DetecterFilter("DetecterFilter", "nombres");
        proxy.addFilter(dec);

        Filter f = proxy.getFilter("DetecterFilter");

        Assert.assertEquals("DetecterFilter", f.getName());
    }

    @Test
    public final void getNotExistingClass() {

        Filter f = proxy.getFilter("DetecterFilter");

        Assert.assertEquals("DetecterFilter", f.getName());
    }

    @Test
    public final void nullClass() {

        Filter f = proxy.getFilter("Detecter");

        Assert.assertEquals(null, f);
    }

}
