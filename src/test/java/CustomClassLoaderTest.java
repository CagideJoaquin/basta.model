import basta.util.CustomClassLoader;
import org.junit.Assert;
import org.junit.Test;

public class CustomClassLoaderTest {

    private CustomClassLoader cl;

    public CustomClassLoaderTest() {
        this.cl = new CustomClassLoader();
    }

    @Test
    public final void loadClass() {

        String clase = "basta.filters.TranslateFilter";
        try {
            Class c = cl.findClass(clase);
            Assert.assertEquals(clase, c.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public final void notFoundClass() {

        String clase = "TranslateFilter";
        Class c = null;
        try {
            c = cl.findClass(clase);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Assert.assertNull(c);
    }

    @Test
    public final void nullNameClass() {

        String clase = null;
        Class c = null;
        try {
            c = cl.findClass(clase);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Assert.assertNull(c);
    }
}



