package basta.util;

public class ConfigurationMnemonic {

    public static String TOKEN_KEY = "TOKEN_KEY";
    public static  String CONSUMER_KEY = "CONSUMER_KEY";
    public static  String CONSUMER_SECRET = "CONSUMER_SECRET";
    public static  String TOKEN_SECRET = "TOKEN_SECRET";

}
