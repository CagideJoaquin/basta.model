package basta.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadSequence {

    private static final String SEPARATOR = ",";

    public List<String> readFilters(String fileName) throws IOException {
        List<String> filters = new ArrayList<>();
        BufferedReader br = null;

        br = new BufferedReader(new FileReader(fileName));
        String line = br.readLine();

        while (null != line) {
            String[] fields = line.split(SEPARATOR);

            filters.addAll(Arrays.asList(fields));

            line = br.readLine();
        }

        if (br != null) {

            br.close();

        }
        return filters;
    }
}