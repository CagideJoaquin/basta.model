package basta.util;

import basta.structure.Filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilterConfig {

    public Filter configureFilter(Filter f, String tipo) {
        if (f.getName().equals("DetecterFilter")) {
            f.configureFilter(getDetecterFilter(tipo));
            f.setType(tipo);
            return f;
        } else if (f.getName().equals("TranslateFilter")) {
            f.configureFilter(getTranslateFilter(tipo));
            f.setType(tipo);
            return f;
        } else if (f.getName().equals("SentimentFilter")) {
            f.setType(tipo);
            return f;
        }else if (f.getName().equals("WindowSentimentAnalysisFilter")) {
            f.setType(tipo);
            return f;
        }
        return null;
    }

    public Map<Character, Character> getTranslateFilter(String tipo) {
        if (tipo == "vocals") {
            HashMap<Character, Character> vocals = new HashMap<>();
            vocals.put('á', 'a');
            return vocals;
        } else if (tipo == "mayus") {
            HashMap<Character, Character> mayus = new HashMap<>();
            mayus.put('A', 'a');
            return mayus;
        }
        return new HashMap<>();
    }

    public List<String> getDetecterFilter(String tipo) {
        if (tipo == "nombres") {
            ArrayList<String> nombres = new ArrayList<>();
            nombres.add("julia");
            return nombres;
        } else if (tipo == "negras") {
            ArrayList<String> negras = new ArrayList<>();
            negras.add("matar");
            return negras;
        }
        return new ArrayList<>();
    }
}