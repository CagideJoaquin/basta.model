package basta.exporter;

import basta.structure.Message;

public class Exporter {

    private IStrategyExport strategy;

    public Exporter(IStrategyExport strategy) {
        this.strategy = strategy;
    }

    public void export(Message message) {
        if (message != null) {
            strategy.export(message);
        }
    }

    public IStrategyExport getStrategy()
    {
        return  this.strategy;
    }
}
