package basta.connection.netviews;

import basta.connection.IPanel;
import basta.util.NetNameMnemonic;
import basta.connection.netconfiguration.Configuration;
import basta.util.ConfigurationMnemonic;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.*;
import javax.swing.border.BevelBorder;

public class TwitterPanel implements IPanel {
    private JPanel contentPane;
    private JTextField txfKeyData;
    private JTextField txfSecretData;
    private JTextField txfTokenKeyData;
    private JTextField txfTokenSecretData;

    public TwitterPanel() {

        contentPane = new JPanel();
        contentPane.setBounds(0, 0, 369, 318);
        contentPane.setBackground(Color.LIGHT_GRAY);
        contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK));
        contentPane.setLayout(null);

        JLabel lblTitle = new JLabel("Configuraci�n de Twitter");
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitle.setForeground(Color.BLACK);
        lblTitle.setFont(new Font("Arial", Font.BOLD, 16));
        lblTitle.setBackground(SystemColor.inactiveCaptionBorder);
        lblTitle.setBounds(10, 10, 349, 49);
        getPanel().add(lblTitle);

        JSeparator separator = new JSeparator();
        separator.setBounds(10, 71, 349, 2);
        contentPane.add(separator);

        JLabel lblKeyData = new JLabel("Key Data");
        lblKeyData.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblKeyData.setBounds(10, 96, 130, 14);
        contentPane.add(lblKeyData);

        JLabel lblSecretData = new JLabel("Secret Data");
        lblSecretData.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblSecretData.setBounds(10, 121, 130, 14);
        contentPane.add(lblSecretData);

        JLabel lblTokenKeyData = new JLabel("Token Key Data");
        lblTokenKeyData.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblTokenKeyData.setBounds(10, 146, 130, 14);
        contentPane.add(lblTokenKeyData);

        JLabel lblTokenSecretData = new JLabel("Token Secret Data");
        lblTokenSecretData.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblTokenSecretData.setBounds(10, 171, 130, 14);
        contentPane.add(lblTokenSecretData);

        txfKeyData = new JTextField();
        txfKeyData.setFont(new Font("Tahoma", Font.BOLD, 12));
        txfKeyData.setBounds(150, 93, 209, 20);
        contentPane.add(txfKeyData);
        txfKeyData.setColumns(10);

        txfSecretData = new JTextField();
        txfSecretData.setFont(new Font("Tahoma", Font.BOLD, 12));
        txfSecretData.setColumns(10);
        txfSecretData.setBounds(150, 118, 209, 20);
        contentPane.add(txfSecretData);

        txfTokenKeyData = new JTextField();
        txfTokenKeyData.setFont(new Font("Tahoma", Font.BOLD, 12));
        txfTokenKeyData.setColumns(10);
        txfTokenKeyData.setBounds(150, 143, 209, 20);
        contentPane.add(txfTokenKeyData);

        txfTokenSecretData = new JTextField();
        txfTokenSecretData.setFont(new Font("Tahoma", Font.BOLD, 12));
        txfTokenSecretData.setColumns(10);
        txfTokenSecretData.setBounds(150, 168, 209, 20);
        contentPane.add(txfTokenSecretData);

    }

    @Override
    public JPanel getPanel() {
        return contentPane;
    }

    @Override
    public String getName() {
        return NetNameMnemonic.TWITTER;
    }


    @Override
    public void setConfiguration(Configuration config) {
        if (config != null) {
            this.txfKeyData.setText(config.getParameter(ConfigurationMnemonic.CONSUMER_KEY));
            this.txfSecretData.setText(config.getParameter(ConfigurationMnemonic.CONSUMER_SECRET));
            this.txfTokenKeyData.setText(config.getParameter(ConfigurationMnemonic.TOKEN_KEY));
            this.txfTokenSecretData.setText(config.getParameter(ConfigurationMnemonic.TOKEN_SECRET));
        }
    }


    @Override
    public Configuration getConfiguration() {
        Configuration config = new Configuration();
        String cKey = this.txfKeyData.getText();
        String cSecret = this.txfSecretData.getText();
        String tKey = this.txfTokenKeyData.getText();
        String tSecret = this.txfTokenSecretData.getText();

        if (!cKey.equals("") & !cSecret.equals("") & !tKey.equals("") & !tSecret.equals("")) {
            config.putParameter(ConfigurationMnemonic.CONSUMER_KEY, cKey);
            config.putParameter(ConfigurationMnemonic.CONSUMER_SECRET, cSecret);
            config.putParameter(ConfigurationMnemonic.TOKEN_KEY, tKey);
            config.putParameter(ConfigurationMnemonic.TOKEN_SECRET, tSecret);
        }
        return config;
    }
}