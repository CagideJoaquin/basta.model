package basta.connection;

import basta.connection.netconfiguration.Configuration;

import javax.swing.*;

public interface IPanel {

    public JPanel getPanel();

    public Configuration getConfiguration();

    public void setConfiguration(Configuration config);

    public String getName();
}