package basta.connection;

import basta.util.LocationMnemonic;

import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.io.File;
import java.io.IOException;

public class Discovery extends ClassLoader {

    public void searchResources() throws Exception {

        List<Class> clases = getClasses(LocationMnemonic.NETWORKS);
        List<ISocialNetwork> networks = new ArrayList<>();
        List<IPanel> IPanels = new ArrayList<>();
        Constructor cons;

        for (Class clazz : clases) {
            if (ISocialNetwork.class.isAssignableFrom(clazz)) {
                cons = clazz.getConstructor();
                ISocialNetwork s = (ISocialNetwork) cons.newInstance();
                networks.add(s);
            }
        }
        for (ISocialNetwork s : networks) {
            NetRegistry.getInstance().put(s.getName(), s);
        }

        clases = getClasses(LocationMnemonic.NETVIEWS);
        for (Class clazz : clases) {
            if (IPanel.class.isAssignableFrom(clazz)) {
                cons = clazz.getConstructor();
                IPanel p = (IPanel) cons.newInstance();
                IPanels.add(p);
            }
        }
        for (IPanel p : IPanels) {
            PanelRegistry.getInstance().put(p.getName(), p);
        }
    }

    private List<Class> getClasses(String pack) {

        ArrayList<Class> classes = new ArrayList<>();
        JarFile jf = null;
        try {
            String s = ((new File(this.getClass().getResource("").getPath())).getParent().replaceAll("file:", ""));
            s = s.substring(0, s.length() - 7);
            jf = new JarFile(s);

            URL[] urls = {new URL("jar:file:" + s + "!/")};
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            Enumeration<JarEntry> entries = jf.entries();
            while (entries.hasMoreElements()) {
                JarEntry je = entries.nextElement();
                if (je.getName().contains(pack) && je.getName().endsWith(".class")) {
                    //String clazz = je.getName();
                    //classes.add(Class.forName(clazz));
                    String className = je.getName().substring(0, je.getName().length() - 6);
                    className = className.replace('/', '.');
                    Class c = cl.loadClass(className);
                    classes.add(c);
                }
            }

            return classes;
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (jf != null)
                    jf.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return classes;
    }
}