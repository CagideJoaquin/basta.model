package basta.connection.networks;

import basta.connection.ISocialNetwork;
import basta.connection.netconfiguration.Configuration;
import basta.structure.Message;
import basta.structure.channel.pubsubchannel.Event;
import basta.util.ConfigurationMnemonic;
import basta.util.NetNameMnemonic;
import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;


public class PageTwitter implements ISocialNetwork {

    private Twitter connection;
    private Configuration configuration;

    public PageTwitter() {
    }

    @Override
    public void connect() {
        if (configuration != null) {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(configuration.getParameter(ConfigurationMnemonic.CONSUMER_KEY));
            builder.setOAuthConsumerSecret(configuration.getParameter(ConfigurationMnemonic.CONSUMER_SECRET));
            builder.setOAuthAccessToken(configuration.getParameter(ConfigurationMnemonic.TOKEN_KEY));
            builder.setOAuthAccessTokenSecret(configuration.getParameter(ConfigurationMnemonic.TOKEN_SECRET));
            twitter4j.conf.Configuration configurationBuilder = builder.build();
            TwitterFactory factory = new TwitterFactory(configurationBuilder);
            connection = factory.getInstance();


        }
    }

    @Override
    public String getName() {
        return NetNameMnemonic.TWITTER;
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }

    @Override
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Twitter getConnection() {
        return connection;
    }

    @Override
    public void getMessagesStream() throws TwitterException {
        int PAGE = 1;
        int COUNT = 5;

            ResponseList statusReponseList = getConnection().getUserTimeline(new Paging(PAGE, COUNT));
            for (Object s : statusReponseList) {

                Status status = (Status) s;

                Message message = new Message();
                message.setBody(status.getText());
                message.addAttribute("date", String.valueOf(status.getCreatedAt()));
                message.addAttribute("sequenceName", getName());


                Event.operation.publish(getName(), message);

            }


    }
}