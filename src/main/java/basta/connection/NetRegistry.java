package basta.connection;

import basta.connection.ISocialNetwork;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

public class NetRegistry {

    private static Map<String, ISocialNetwork> INSTANCE;

    public static Map<String, ISocialNetwork> getInstance() {

        if (INSTANCE != null) {
            return INSTANCE;
        } else {
            INSTANCE = new HashMap<>();
            return INSTANCE;
        }
    }
}
