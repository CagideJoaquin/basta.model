package basta.connection;

import basta.connection.netconfiguration.Configuration;
import twitter4j.TwitterException;

public interface ISocialNetwork {

    void connect() throws TwitterException;

    String getName();

    void getMessagesStream() throws TwitterException;

    Configuration getConfiguration();

    void setConfiguration(Configuration config);
}