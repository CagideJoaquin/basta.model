package basta.connection;

import java.util.HashMap;
import java.util.Map;

public class PanelRegistry
{
    private static Map<String, IPanel> INSTANCE;

    public static Map<String, IPanel> getInstance() {

        if (INSTANCE != null) {
            return INSTANCE;
        } else {
            INSTANCE = new HashMap<>();
            return INSTANCE;
        }
    }
}