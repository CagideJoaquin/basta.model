package basta.filters;

import basta.structure.*;

import java.util.ArrayList;
import java.util.List;

public class DetecterFilter extends Filter implements IConfigurator {

    private List<String> tokens;
    private IStrategyDetect strategy;

    public DetecterFilter(String name, String type) {
        super(name, type);
        strategy = new StrategyDetectWord();
    }

    @Override
    public void configureFilter(Object options) {
        this.tokens = (ArrayList<String>) options;
    }

    @Override
    public void filter() {

        if(!this.getIQueueIn().isEmpty()){

        for (int i = 0; i < this.getIQueueIn().size(); i++) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                if (this.strategy.detect(message, tokens)) {
                    this.getIQueueOut().put(message);
                    this.filter();
                }
            }
        }
        }
    }

    public void changeStrategy(IStrategyDetect strate) {
        this.strategy = strate;
    }
}
