package basta.filters;

import basta.exporter.Exporter;
import basta.structure.Filter;
import basta.structure.Message;

import java.util.HashMap;

public class WindowSentimentAnalysisFilter extends Filter {

    private static final String SENTIMENT = "sentiment";


    public WindowSentimentAnalysisFilter(String name, String type) {
        super(name, type);
    }

    @Override
    public void filter() {

        if(!this.getIQueueIn().isEmpty() && this.getIQueueIn().size() >= 3) {
            int count = 0;
            Message message = null;
            int sentimentCount=0;

            for (int i = 0; i < 3; i++) {

                message = this.getIQueueIn().getMessage();

                if (message != null) {
                    sentimentCount = Integer.parseInt(message.getHeaderMessage().getAttributes().get(SENTIMENT));
                    count = count + sentimentCount;
                }
            }

            if(count < 0 ) {

                message.setBody("ALERT : " + count);
                message.getHeaderMessage().setAttributes(new HashMap<>());


                this.getIQueueOut().put(message);
            }

            this.filter();
        }
    }



}
