package basta.filters;

import basta.structure.Filter;
import basta.util.CustomClassLoader;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class FilterClassConstructor implements IFilterRetriever {

    private CustomClassLoader cl;

    public FilterClassConstructor() {
        this.cl = new CustomClassLoader();
    }

    @Override
    public Filter getFilter(String name) {

        try {
            Class c = cl.findClass("basta.filters." + name);
            Constructor cons = c.getConstructor(String.class, String.class);
            return (Filter) cons.newInstance(name, "");
        } catch (ClassNotFoundException | NullPointerException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            return null;
        }
    }
}
