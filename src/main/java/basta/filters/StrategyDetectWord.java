package basta.filters;

import basta.structure.Message;

import java.util.List;

public class StrategyDetectWord implements IStrategyDetect {

    @Override
    public boolean detect(Message message, List<String> tokens) {

        if (tokens.size() == 0)
            return true;

        for (int i = 0; i < tokens.size(); i++) {
            CharSequence sequence = tokens.get(i);
            if (message.getBody().contains(sequence)) {
                return true;
            }
        }

        return false;
    }
}
