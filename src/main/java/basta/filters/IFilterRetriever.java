package basta.filters;

import basta.structure.Filter;

public interface IFilterRetriever {
    Filter getFilter(String name);
}
