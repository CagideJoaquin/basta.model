package basta.filters;

import basta.structure.Message;

import java.util.ArrayList;
import java.util.List;

public class StrategyDetectChars implements IStrategyDetect {

    @Override
    public boolean detect(Message message, List<String> tokens) {

        if (tokens.size() == 0)
            return true;

        CharSequence sequence;

        for (int i = 0; i < tokens.size(); i++) {
            String a = tokens.get(i);
            if (a.length() >= 3) {
                sequence = a.substring(0, 3);
                if (message.getBody().contains(sequence)) {
                    return true;
                }
            }
        }
        return false;
    }
}
