package basta.filters;

import java.util.List;

import basta.structure.Message;

public interface IStrategyDetect {

    public boolean detect(Message message, List<String> tokens);
}
