package basta.filters;

import basta.structure.Filter;
import basta.structure.Message;
import basta.structure.channel.pubsubchannel.OnMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TranslateFilter extends Filter implements IConfigurator {

    private Map<Character, Character> mapeos;

    public TranslateFilter(String name, String type) {
        super(name, type);
    }

    @Override
    public void filter() {

        if(!this.getIQueueIn().isEmpty()) {

            char character;
            char[] chain;

            for (int i = 0; i < this.getIQueueIn().size(); i++) {

                Message message = this.getIQueueIn().getMessage();

                if (message != null) {

                    chain = message.getBody().toCharArray();

                    for (int j = 0; j < chain.length; j++) {

                        character = chain[j];

                        if (mapeos.containsKey(character)) {
                            chain[j] = mapeos.get(character);
                        }
                    }
                    String aux = String.valueOf(chain);
                    message.setBody(aux);

                    this.getIQueueOut().put(message);
                    this.filter();
                }
            }
        }
    }

    @Override
    public void configureFilter(Object options) {
        this.mapeos = (Map<Character, Character>) options;
    }
}
