package basta.filters;

public interface IConfigurator {

    void configureFilter(Object options);
}