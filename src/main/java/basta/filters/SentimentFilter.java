package basta.filters;

import basta.inicialization.BastaModel;
import basta.structure.Filter;
import basta.structure.Message;

public class SentimentFilter extends Filter implements IConfigurator {

    public SentimentFilter(String name, String type) {
        super(name, type);
    }

    @Override
    public void filter() {


        if(!this.getIQueueIn().isEmpty()) {
            for (int i = 0; i < this.getIQueueIn().size(); i++) {

                Message message = this.getIQueueIn().getMessage();

                if (message != null) {

                    message.addAttribute("sentiment",
                            BastaModel.getInstance(false).getCoreAnalizer().analyzeText(message.getBody()).toString());

                    this.getIQueueOut().put(message);

                }
            }
        }



    }
}
