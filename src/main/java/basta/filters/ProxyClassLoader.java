package basta.filters;

import basta.structure.Filter;

import java.util.ArrayList;
import java.util.List;

public class ProxyClassLoader implements IFilterRetriever {

    private List<Filter> filters;
    private IFilterRetriever clasC;

    public ProxyClassLoader() {
        this.filters = new ArrayList<>();
        this.clasC = new FilterClassConstructor();
    }

    public void addFilter(Filter filter) {
        this.filters.add(filter);
    }

    @Override
    public Filter getFilter(String name) {
        for (Filter f : filters) {
            if (f.getName().equals(name))
                return f;
        }

        Filter f = clasC.getFilter(name);

        if (f != null) {
            addFilter(f);
            return f;
        }
        return null;
    }
}