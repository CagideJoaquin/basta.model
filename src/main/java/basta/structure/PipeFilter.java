package basta.structure;

import basta.structure.channel.IQueue;
import basta.structure.channel.Queue;
import basta.structure.channel.pubsubchannel.OnMessage;
import java.util.List;
import java.util.ArrayList;

public class PipeFilter implements IFilter {

    private List<IFilter> filters;
    private IQueue iQueueIn;
    private IQueue iQueueOut;

    public PipeFilter() {
        this.filters = new ArrayList<>();
        this.iQueueIn = new Queue();
        this.iQueueOut = new Queue();
    }

    public void add(IFilter filter) {

        if (!this.filters.isEmpty()) {

            this.filters.get(this.filters.size() - 1).setIQueueOut(filter.getIQueueIn());
            this.filters.add(filter);

        } else {
            this.filters.add(filter);
        }

    }

    public List<IFilter> getFilters()
    {
        return this.filters;
    }

    @Override
    public void filter() {
        if (!this.filters.isEmpty() && !this.iQueueIn.isEmpty())
        {
            this.filters.get(0).getIQueueIn().put(this.getIQueueIn().getMessage());
            this.filters.get(this.filters.size()-1).setIQueueOut(this.getIQueueOut());

            for(int i=0;i<filters.size();i++)
            {
                this.filters.get(i).filter();
                this.filter();
            }
        }
    }

    @Override
    public IQueue getIQueueIn() {
        return this.iQueueIn;
    }

    @Override
    public void setIQueueIn(IQueue iQueue) {
        this.iQueueIn = iQueue;
    }

    @Override
    public IQueue getIQueueOut() {
        return this.iQueueOut;
    }

    @Override
    public void setIQueueOut(IQueue iQueue) {
        this.iQueueOut = iQueue;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getType() {
        return null;
    }

    @OnMessage
    public void onMessage(Message message) {
        this.iQueueIn.put(message);
    }
}
