package basta.structure;

import basta.filters.IFilterRetriever;
import basta.filters.ProxyClassLoader;
import basta.util.FilterConfig;

public class FilterFactory {

    private IFilterRetriever proxy;
    private FilterConfig config;

    public FilterFactory() {
        proxy = new ProxyClassLoader();
        config = new FilterConfig();
    }

    public Filter getFilter(String name, String tipo) {
        Filter f = proxy.getFilter(name);
        config.configureFilter(f, tipo);

        return f;
    }
}
