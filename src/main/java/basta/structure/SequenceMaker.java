package basta.structure;

import basta.util.ReadSequence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SequenceMaker {

    private ReadSequence readSequence;
    private FilterFactory filterFactory;

    public SequenceMaker() {
        readSequence = new ReadSequence();
        filterFactory = new FilterFactory();
    }

    public Sequencer makeSequence(String filename) throws IOException {
        List<String> csvFilter = readSequence.readFilters(filename);

        List<Filter> filters = new ArrayList<>();

        for (int i = 0; i < csvFilter.size(); i = i + 2) {
            Filter f = filterFactory.getFilter(csvFilter.get(i), csvFilter.get(i + 1));
            if (f != null & noRepeated(filters, f)) {
                filters.add(f);
            }
        }
        return new Sequencer(filters);
    }

    private boolean noRepeated(List<Filter> filters, Filter f) {

        for (Filter filter : filters) {
            if (filter.getName().equals(f.getName()) & filter.getType().equals(f.getType())) {
                return false;
            }
        }
        return true;
    }
}