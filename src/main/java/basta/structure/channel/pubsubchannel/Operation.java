package basta.structure.channel.pubsubchannel;

import basta.structure.Message;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Operation extends Event {


    public void subscribe(String channelName, Object subscriber) {
        if (!channels.containsKey(channelName)) {
            channels.put(channelName, new HashMap<>());
        }

        channels.get(channelName).put(subscriber.hashCode(), subscriber);
    }

    public void publish(String channelName, Message message) {
        Map channel = channels.get(channelName);
        if (channel != null) {
            for (Map.Entry<Integer, Object> subs : channels.get(channelName).entrySet()) {
                Object subscriberObj = subs.getValue();

                for (final Method method : (subscriberObj).getClass().getDeclaredMethods()) {
                    Annotation annotation = method.getAnnotation(OnMessage.class);

                    if (annotation != null) {

                        deliverMessage(subscriberObj, method, message);

                    }
                }
            }
        }
    }

    public void deliverMessage(Object subscriber, Method method, Message message) {
        try {
            boolean methodFound = false;
            for (final Class paramClass : method.getParameterTypes()) {
                if (paramClass.equals(message.getClass())) {
                    methodFound = true;
                    break;
                }
            }
            if (methodFound) {
                method.setAccessible(true);
                method.invoke(subscriber, message);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

