package basta.structure.channel.pubsubchannel;

import java.util.HashMap;
import java.util.Map;

public class Event {
    static {
        init();
    }

    public static Operation operation;

    public static Map<String, Map<Integer, Object>> channels;

    static void init() {
        channels = new HashMap<>();
        operation = new Operation();
    }
}