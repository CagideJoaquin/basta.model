package basta.structure;

import basta.filters.IConfigurator;
import basta.structure.channel.IQueue;
import basta.structure.channel.Queue;

public class Filter implements IFilter, INameable, IConfigurator {

    private IQueue queueIn;
    private IQueue queueOut;
    private String name;
    private String type;

    public Filter(String name, String type) {
        this.name = name;
        this.type = type;
        this.queueIn = new Queue();
        this.queueOut = new Queue();
    }

    @Override
    public void filter(){

    }

    @Override
    public IQueue getIQueueIn() {
        return this.queueIn;
    }

    @Override
    public void setIQueueIn(IQueue iQueue) {
        this.queueIn = iQueue;
    }

    @Override
    public IQueue getIQueueOut() {
        return this.queueOut;
    }

    @Override
    public void setIQueueOut(IQueue iQueue) {
        this.queueOut = iQueue;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Filter{ name='" + name + '\'' + ", type='" + type + '\'' +
                '}';
    }

    @Override
    public void configureFilter(Object options) {

    }
}
