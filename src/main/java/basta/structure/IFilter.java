package basta.structure;

import basta.structure.channel.IQueue;

public interface IFilter {

    public void  filter();

    public IQueue getIQueueIn();

    public void setIQueueIn(IQueue iQueue);

    public IQueue getIQueueOut();

    public void setIQueueOut(IQueue iQueue);

    public String getName();

    public String getType();

}
