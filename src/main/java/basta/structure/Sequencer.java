package basta.structure;

import java.util.List;

public class Sequencer {

    private PipeFilter pipeFilter = new PipeFilter();

    public Sequencer(List<Filter> filters) {

        if (filters != null) {
            for (int i = 0; i < filters.size(); i++) {
                pipeFilter.add(filters.get(i));
            }
        }
    }

    public PipeFilter getPipeFilter() {
        return this.pipeFilter;
    }
}

