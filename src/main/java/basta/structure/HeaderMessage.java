package basta.structure;

import java.util.HashMap;
import java.util.Map;

public class HeaderMessage {

    private Map<String, String> attributes;

    public HeaderMessage() {
        this.attributes = new HashMap<String, String>();
    }

    public void addAttribute(String key, String value) {
        this.attributes.put(key, value);
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Map<String,String> getAttributes()
    {
        return this.attributes;
    }

    @Override
    public String toString() {
        return attributes.toString();
    }
}
