package basta.inicialization;

import basta.connection.Discovery;
import basta.sentimentanalysis.CoreAnalizer;

import java.util.Properties;

public class BastaModel {

    private CoreAnalizer coreAnalizer;
    private static BastaModel instance = null;
    private Discovery discovery = new Discovery();
    private static Boolean doNetDiscover;

    public static BastaModel getInstance(Boolean netDiscover)  {

        if (instance == null) {
            instance = new BastaModel();
            doNetDiscover = netDiscover;
        }

        return instance;
    }

    public void init()  {

        if (doNetDiscover) {

            try {
                discovery.searchResources();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (coreAnalizer == null) {
            initAnalizer();
        }

    }

    private void initAnalizer() {

        Properties props = new Properties();

        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, parse, sentiment");

        this.coreAnalizer = new CoreAnalizer(props);

    }

    public CoreAnalizer getCoreAnalizer() {

        return coreAnalizer;
    }

}
