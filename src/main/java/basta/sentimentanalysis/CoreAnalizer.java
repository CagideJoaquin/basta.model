package basta.sentimentanalysis;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;

import java.util.List;
import java.util.Properties;

public class CoreAnalizer implements IAnalizer {

    private Properties properties;
    private StanfordCoreNLP pipeline;

    public final static String POSITIVE = "Positive";
    public final static String NEGATIVE = "Negative";
    public final static String NEUTRAL = "Neutral";

    public CoreAnalizer(Properties properties){

         this.pipeline = new StanfordCoreNLP(properties);

    }

    @Override
    public Integer analyzeText(String text) {

        Annotation annotation = this.pipeline.process(text);
        int result = 1;

        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);

        for (CoreMap sentence : sentences) {
            String sentiment = sentence.get(SentimentCoreAnnotations.SentimentClass.class);

            result = getSentimentCode(sentiment);
        }

        return result;
    }

    private int getSentimentCode(String sentiment) {

        switch (sentiment){
            case POSITIVE:
                return 1;
            case NEGATIVE:
                return -1;
            default:
                return 0;
        }

    }

    private String getSentimentMnemonic(int code) {

        switch (code){
            case 1:
                return POSITIVE;
            case -1:
                return NEGATIVE;
            default:
                return NEUTRAL;
        }

    }
}
