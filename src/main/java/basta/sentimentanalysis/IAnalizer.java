package basta.sentimentanalysis;


public interface IAnalizer {


    /**
     * @param text String for the analysis
     * @return Values : 1 , 0 , -1 where 1 is positive, 0 is neutral, and -1 is negative.
     */
    Integer analyzeText(String text);

}
